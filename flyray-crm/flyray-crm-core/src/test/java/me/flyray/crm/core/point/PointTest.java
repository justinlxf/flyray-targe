package me.flyray.crm.core.point;

import com.alibaba.fastjson.JSON;
import io.micrometer.core.instrument.util.JsonUtils;
import lombok.extern.slf4j.Slf4j;
import me.flyray.common.msg.TableResultResponse;
import me.flyray.common.vo.QueryPersonalAccountRequest;
import me.flyray.common.vo.QueryPersonalAccountResponse;
import me.flyray.common.vo.QueryPersonalPointRequest;
import me.flyray.crm.core.FlyrayCrmBootstrap;
import me.flyray.crm.core.biz.personal.PersonalAccountBiz;
import me.flyray.crm.core.entity.PersonalPointJournal;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = FlyrayCrmBootstrap.class)
@Slf4j
public class PointTest {

    @Autowired
    private PersonalAccountBiz personalAccountBiz;

    @Test
    public void test(){
        QueryPersonalAccountRequest queryPersonalAccountRequest = new QueryPersonalAccountRequest();
        queryPersonalAccountRequest.setPersonalId("101");
        //QueryPersonalAccountResponse queryPersonalAccountResponse = personalAccountBiz.queryPersonalAcount(queryPersonalAccountRequest);
        //log.info(JSON.toJSONString(queryPersonalAccountResponse));
    }

    @Test
    @Ignore
    public void test2(){
        QueryPersonalPointRequest queryPersonalAccountRequest = new QueryPersonalPointRequest();
        queryPersonalAccountRequest.setPersonalId("1");
        //TableResultResponse<PersonalPointJournal> tableResultResponse = personalAccountBiz.queryPointList(queryPersonalAccountRequest);
        //log.info(JSON.toJSONString(tableResultResponse));
    }


}
