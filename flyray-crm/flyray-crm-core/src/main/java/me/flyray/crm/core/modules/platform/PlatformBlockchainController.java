package me.flyray.crm.core.modules.platform;

import me.flyray.crm.core.biz.platform.PlatformBlockchainBiz;
import me.flyray.crm.core.entity.PlatformBlockchain;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import me.flyray.common.rest.BaseController;

@Controller
@RequestMapping("platformBlockchain")
public class PlatformBlockchainController extends BaseController<PlatformBlockchainBiz, PlatformBlockchain> {

}