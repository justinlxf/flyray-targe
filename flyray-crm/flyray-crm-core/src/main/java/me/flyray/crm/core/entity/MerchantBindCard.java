package me.flyray.crm.core.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;


/**
 * 
 * 
 * @author admin
 * @email ${email}
 * @date 2019-01-07 17:41:33
 */
@Data
@Table(name = "merchant_bind_card")
public class MerchantBindCard implements Serializable {
	private static final long serialVersionUID = 1L;
	
	    //
    @Id
    private Long id;
	
	    //平台号
    @Column(name = "platform_id")
    private String platformId;
	
	    //商户ID
    @Column(name = "merchant_id")
    private String merchantId;
	
	    //0:个人 1：企业
    @Column(name = "merchant_type")
    private String merchantType;
	
	    //身份证号
    @Column(name = "id_card_no")
    private String idCardNo;
	
	    //身份证正面照片
    @Column(name = "id_card_front_img")
    private String idCardFrontImg;
	
	    //身份证反面照片
    @Column(name = "id_card_back_img")
    private String idCardBackImg;
	
	    //持卡人姓名
    @Column(name = "cardholder_name")
    private String cardholderName;
	
	    //收款银行名称
    @Column(name = "beneficiary_bank_name")
    private String beneficiaryBankName;
	
	    //收款人卡号
    @Column(name = "beneficiary_bank_card_no")
    private String beneficiaryBankCardNo;
	
	    //加密银行卡号
    @Column(name = "encrypt_bank_card_no")
    private String encryptBankCardNo;
	
	    //状态  00：待验证  01：绑定  02：解绑 
    @Column(name = "status")
    private String status;
	
	    //收款银行支行名称
    @Column(name = "beneficiary_bank_branch_name")
    private String beneficiaryBankBranchName;
	
	    //支行编号
    @Column(name = "beneficiary_branch_no")
    private String beneficiaryBranchNo;
	
	    //创建时间
    @Column(name = "create_time")
    private Date createTime;
	
	    //更新时间
    @Column(name = "update_time")
    private Date updateTime;

    /**
     * 为了区分不同角色的商户
     */
    @Column(name = "role_type")
    private String roleType;

    /**
     * 开户许可证
     */
    @Column(name = "account_permits_img")
    private String accountPermitsImg;

}
