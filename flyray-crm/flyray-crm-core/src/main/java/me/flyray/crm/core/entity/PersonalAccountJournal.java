package me.flyray.crm.core.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import me.flyray.common.entity.BaseEntity;


/**
 * 个人账户流水（充、转、提、退、冻结流水）
 * 
 * @author centerroot
 * @email ${email}
 * @date 2018-07-16 10:15:49
 */
@Data
@Table(name = "personal_account_journal")
public class PersonalAccountJournal extends BaseEntity implements Serializable {
	private static final long serialVersionUID = 1L;
	
	    //流水号
    @Id
    private String journalId;
	
	    //平台编号
    @Column(name = "platform_id")
    private String platformId;
	
	    //个人信息编号
    @Column(name = "personal_id")
    private String personalId;
	
	    //账户编号
    @Column(name = "account_id")
    private String accountId;
	
	    //账户类型    ACC001：余额账户
    @Column(name = "account_type")
    private String accountType;
	
	    //订单号
    @Column(name = "order_no")
    private String orderNo;
	
	    //来往标志  1：来账   2：往账
    @Column(name = "in_out_flag")
    private String inOutFlag;
	
	    //交易金额
    @Column(name = "trade_amt")
    private BigDecimal tradeAmt;
	
	    //交易类型  支付:01，退款:02，提现:03，充值:04, 发表说说领取：05, 分享领取: 06
    @Column(name = "trade_type")
    private String tradeType;
	
	    //创建时间
    @Column(name = "create_time")
    private Date createTime;
	
	    //更新时间
    @Column(name = "update_time")
    private Date updateTime;
	
  //积分使用场景
  	@Column(name = "scene_id")
  	private String sceneId;

	/**
	 * 批量操作批次号
	 */
	@Column(name = "batch_no")
	private String batchNo;

	/**
	 * 备注
	 */
	@Column(name = "remark")
	private String remark;

}
