package me.flyray.crm.core.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import me.flyray.common.entity.BaseEntity;


/**
 * 个人账户信息
 *
 * @author centerroot
 * @email ${email}
 * @date 2018-07-16 10:15:49
 */
@Table(name = "personal_account")
@Data
public class PersonalPointAccount extends BaseEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    //账户编号
    @Column(name = "account_id")
    private String accountId;

    //平台编号
    @Column(name = "platform_id")
    private String platformId;

    //个人信息编号
    @Column(name = "personal_id")
    private String personalId;

    //积分账户
    @Column(name = "point")
    private BigDecimal point;

    //历年积分账户
    @Column(name = "his_point")
    private BigDecimal hisPoint;

}
