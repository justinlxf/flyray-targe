package me.flyray.crm.core.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import me.flyray.common.entity.BaseEntity;


/**
 * 商户账户流水（充、转、提、退、冻结流水）
 * 
 * @author centerroot
 * @email ${email}
 * @date 2018-07-16 10:15:49
 */
@Data
@Table(name = "merchant_account_journal")
public class MerchantAccountJournal extends BaseEntity implements Serializable {
	private static final long serialVersionUID = 1L;
	
	    //流水号
    @Id
    private String journalId;
	
	    //平台编号
    @Column(name = "platform_id")
    private String platformId;

    //客户编号
    @Column(name = "customer_id")
    private String customerId;
	
	    //商户编号
    @Column(name = "merchant_id")
    private String merchantId;

	@Column(name = "merchant_name")
	private String merchantName;
	
    //来向账户
    @Column(name = "from_account")
    private String fromAccount;

	//来向账户名称
	@Column(name = "from_account_name")
	private String fromAccountName;

	//去向账户
	@Column(name = "to_account")
	private String toAccount;

	//去向账户名称
	@Column(name = "to_account_name")
	private String toAccountName;

	//账户类型    ACC001：余额账户
    @Column(name = "account_type")
    private String accountType;
	
	    //订单号
    @Column(name = "order_no")
    private String orderNo;
	
	    //来往标志  1：来账   2：往账 
    @Column(name = "in_out_flag")
    private String inOutFlag;
	
	    //交易金额
    @Column(name = "trade_amt")
    private BigDecimal tradeAmt;
	
	    //交易类型  支付:01，退款:02，提现:03，充值:04
    @Column(name = "trade_type")
    private String tradeType;
	
	    //创建时间
    @Column(name = "create_time")
    private Date createTime;
	
	    //更新时间
    @Column(name = "update_time")
    private Date updateTime;

    /**
     * 供业务拓展使用自定义(与customer_base相应)
     */
    @Column(name = "role_type")
    private Integer roleType;
	
}
