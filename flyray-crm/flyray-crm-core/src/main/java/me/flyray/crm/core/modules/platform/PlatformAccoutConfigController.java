package me.flyray.crm.core.modules.platform;

import me.flyray.common.rest.BaseController;
import me.flyray.crm.core.biz.platform.PlatformAccoutConfigBiz;
import me.flyray.crm.core.entity.PlatformAccoutConfig;
import me.flyray.crm.facade.request.PlatformAccoutConfigRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.validation.Valid;
import java.util.Map;

@Controller
@RequestMapping("platformAccoutConfig")
public class PlatformAccoutConfigController extends BaseController<PlatformAccoutConfigBiz, PlatformAccoutConfig> {

	/**
	 * 根据平台编号查询平台支持账户类型
	 * @author centerroot
	 * @time 创建时间:2018年8月16日上午11:41:30
	 * @param platformId
	 * @return
	 */
	@RequestMapping(value = "/pageList",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> pageList(@RequestBody @Valid PlatformAccoutConfigRequest platformAccoutConfigRequest){
		Map<String, Object> respMap = baseBiz.pageList(platformAccoutConfigRequest);
        return respMap;
    }
	
	/**
	 * 添加支持的账户类型
	 * @author centerroot
	 * @time 创建时间:2018年8月16日下午1:44:08
	 * @param platformAccoutConfigRequest
	 * @return
	 */
	@RequestMapping(value = "/addObj",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> addPlatformAccout(@RequestBody @Valid PlatformAccoutConfigRequest platformAccoutConfigRequest){
		Map<String, Object> respMap = baseBiz.addPlatformAccout(platformAccoutConfigRequest);
        return respMap;
    }
	
}