package me.flyray.crm.core.feignserver;

import java.util.Map;

import javax.validation.Valid;

import me.flyray.crm.core.biz.platform.PlatformCallbackUrlBiz;
import me.flyray.crm.facade.request.PlatformCallbackUrlRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/***
 * 平台回调地址查询接口
 * */
@Api(tags="平台回调地址查询接口")
@Controller
@RequestMapping("feign/callbackUrl")
public class FeignCallbackUrl {
	
	@Autowired
	private PlatformCallbackUrlBiz platformCallbackUrlBiz;
	
	/**
	 * 根据平台查询回调地址
	 * @param 
	 * @return
	 */
	@ApiOperation("根据平台查询回调地址接口")
	@RequestMapping(value = "/query",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> queryCallbackUrlByPlatformId(@RequestBody @Valid PlatformCallbackUrlRequest params){
		Map<String, Object> response = platformCallbackUrlBiz.queryCallbackUrlByPlatformId(params);
		return response;
    }

}
