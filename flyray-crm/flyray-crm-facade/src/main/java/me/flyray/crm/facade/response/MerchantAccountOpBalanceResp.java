package me.flyray.crm.facade.response;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @Auther: luya
 * @Date: 2019-01-17 09:26
 * @Description: 平台账户累计
 */
@Data
public class MerchantAccountOpBalanceResp implements Serializable {

    //平台编号
    private String platformId;

    //客户编号
    private String customerId;

    //币种  CNY：人民币
    private String ccy;

    //账户余额
    private BigDecimal accountBalance;

    //冻结金额
    private BigDecimal freezeBalance;

    //累计金额
    private BigDecimal countBalance;

    public MerchantAccountOpBalanceResp(){

    }

    public MerchantAccountOpBalanceResp(String platformId, String customerId, String ccy, BigDecimal accountBalance, BigDecimal freezeBalance, BigDecimal countBalance) {
        this.platformId = platformId;
        this.customerId = customerId;
        this.ccy = ccy;
        this.accountBalance = accountBalance;
        this.freezeBalance = freezeBalance;
        this.countBalance = countBalance;
    }
}
