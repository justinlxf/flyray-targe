package me.flyray.crm.facade.response;

import lombok.Data;

import java.util.Date;

/**
 * @Author: bolei
 * @date: 15:35 2019/1/11
 * @Description: 客户绑定银行卡返回信息
 */

@Data
public class CustomerBindCardResponse {


    private  Long id;
    //平台号
    private String platformId;

    //商户ID
    private String merchantId;

    //0:个人 1：企业
    private Integer merchantType;

    //身份证号
    private String idCardNo;

    //身份证正面照片
    private String idCardFrontImg;

    //身份证反面照片
    private String idCardBackImg;

    //持卡人姓名
    private String cardholderName;

    //收款银行名称
    private String beneficiaryBankName;

    //收款人卡号
    private String beneficiaryBankCardNo;

    //加密银行卡号
    private String encryptBankCardNo;

    //状态  00：待验证  01：绑定  02：解绑
    private String status;

    //收款银行支行名称
    private String beneficiaryBankBranchName;

    //支行编号
    private String beneficiaryBranchNo;

    //创建时间
    private Date createTime;

    //更新时间
    private Date updateTime;
    /**
     * 营业执照编号
     */
    private String businessLisence;
    /**
     * 营业执照
     */
    private String lisenceImageUrl;
    /**
     * 银行开户许可证
     */
    private String accountPermitsImg;

}
