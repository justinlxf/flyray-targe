package me.flyray.admin.mapper;

import me.flyray.admin.entity.GateLog;
import tk.mybatis.mapper.common.Mapper;

public interface GateLogMapper extends Mapper<GateLog> {
}