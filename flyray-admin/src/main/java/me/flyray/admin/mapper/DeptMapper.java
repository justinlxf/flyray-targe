package me.flyray.admin.mapper;

import me.flyray.admin.entity.Dept;
import tk.mybatis.mapper.common.Mapper;

public interface DeptMapper extends Mapper<Dept> {

	void updateByPlatformId(Dept dept);

	Dept selectByDeptId(String deptId);
}