package me.flyray.gate.filter;

import com.netflix.zuul.ZuulFilter;

/**
 * @Author: Mu
 * @Description: IP白名单信息过滤
 */

public class AccessIpListFilter extends ZuulFilter {
    @Override
    public String filterType() {
        return "pre";
    }

    @Override
    public int filterOrder() {
        return 2;
    }

    @Override
    public boolean shouldFilter() {
        return false;
    }

    @Override
    public Object run() {
    	
        return null;
    }
}
