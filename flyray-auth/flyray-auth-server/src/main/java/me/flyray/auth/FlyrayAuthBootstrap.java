package me.flyray.auth;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

import tk.mybatis.spring.annotation.MapperScan;

/**
 * Created by bolei
 */
@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients
@MapperScan("me.flyray.auth.mapper")
public class FlyrayAuthBootstrap {
    public static void main(String[] args) {
        SpringApplication.run(FlyrayAuthBootstrap.class, args);
    }
}
