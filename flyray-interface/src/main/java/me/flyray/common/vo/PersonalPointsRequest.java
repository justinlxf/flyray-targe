package me.flyray.common.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * 更新客户经理
 * @author centerroot
 * @time 创建时间:2018年7月16日下午4:23:01
 * @description
 */
@Data
@ApiModel(value = "积分管理")
public class PersonalPointsRequest implements Serializable {


	@ApiModelProperty(value = "平台编号")
	private String platformId;

	@NotNull(message = "参数[personalId]不能为空")
	@ApiModelProperty(value = "个人客户编号")
	private String personalId;

	//@NotNull(message = "参数[realName]不能为空")
	@ApiModelProperty(value = "用户名称")
	private String realName;

	//@NotNull(message = "参数[phone]不能为空")
	@ApiModelProperty(value = "手机号")
	private String phone;

	@NotNull(message = "参数[pointsType]不能为空")
	@ApiModelProperty(value = "积分类型")
	private Integer pointsType;

	@NotNull(message = "参数[points]不能为空")
	@ApiModelProperty(value = "积分")
    private Integer  points;

	@ApiModelProperty(value = "备注")
	private String remark;
}
